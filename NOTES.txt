::TODO::
-integrate with niddhog functions
-testing
	-test with two computers, basic networking
	-test nat punchtrough with two computers
	-create dummy master server that is not running in gml

::NAT punchthrough::		
-master server is estb
-c1
	-connects to ms
	-ms responds saying c1 is server
-c2
	-connects to ms
	-ms responds with c1 ip
	-ms tells c1 c2's ip
	-c2 does something to get c1 added as trusted ip.
	-while connection not establish c1 attempt to connect to c2

::FCN WISHLIST::
-from menu
	-start network game (eventually this will contact master server and sort everything out, but right now it's just a place holder)
		-goes to waiting screen and doesn't do anything
	-start networking as server
		-goes to waiting screen
		-creates server_network object
	-start networking as client
		-goes to waiting screen
		-create server_client object
	-connect as spectator
		-goes to waiting screen
		-creates server_client object and sets some parameter to spectator
-waiting screen
-start game function
	-will take the game from waiting screen to the actual game
	
-VCR
	-get current frame
	-add player input (frame, input) (note, input may be in the future)
		-some place for me to handle networking input for the local player (it's probably better if this is not in the VCR function so we can keep our code as separate as possible)

-in game
	-end game
		-called if player disconnects prematurely.
	-request pause game (# frames) 
		-this will be called repeatedly by networking whenever there is a frame offset greater than a given threshold.
	-pause game (# frames) (I'll only call request pause game, but for now, it can just call pause game directly)
		-this will pause the game for # frames, the VCR should still be able to receive inputs during the pause though
		
-other
	-get framerate
		-this should ALWAYS return the same value no matter what the true framerate is or what computer.

::Other issues::
-potential for issues if lag spike occurs between game end and new game start
-how to synchronize menu nonsense?
	-soln 1. independent menus, server can resolve if both players attempt to start at the same time (actually I don't think anything goes wrong if players start independently, maybe match id mismatch issue..)
DONe-client needs to know what its clientid is

::MESSAGE FORMAT::
-list, first entry is type, remaining entry is determined by user
:MASTER SERVER;
-clientidentity
	-master server response when player becomes server
	-args
		-0/1/2 0 is server, 1 is client, 2 is spec
-opponentip
	-master servers response with opponents ip for nat punchthrough
	-args
		-opponents ip
:CLIENT/SERVER:
-clientregister
	-args
		-player //true if player is a player....
-clientack
	-server response to player register
	-args
		-clientid
		-"you" //true if player receiving is this client
		-player //0 spec, 1 1, 2 2
-ping1 
	-ping initiation from server
	-pingid
		-this id is unique to each ping branch
-ping2 //
	-ping respons client to server
	-args
		-pingid
		-client id
		-client time 
			-this need not be true time, in our case
				-game started: (simulated_frames_since_game_start * framerate)
				-menus, credits, etc: 0
-ping3 
	-ping response, this is sent to all clients
	-args
		-pingid
		-client id
		-client ping
		-client t0 approximation = client time - (client ping)/2
	-client action 
		-client_last_ping_average = client_last_ping_average * 4/5 + client ping (or client ping if last_ping_averga = -1, i.e. first time)
		-save in ping history
		-let c0 be self client id, and c1 be opponent client
		-if t0approx(c0) and t0approx(c1) available 
			-then, report_frame_offset(t0approx(c0)-t0approx(c1))
				-game may choose to freeze game
				-remove from ping history

-input
	-report inputs, this originates from one client but is sent to all clients by server
	-args
		-client id
		-client time
		-input map
	-server action
		-relay to all clients
		-also make sure socket id matches client id to prevent spectator hacks
	-client action
		-submit data to VCR or w/e

-synchronize_event (game start event)
	-signals game to begin
	-args
		-<none>
	-client action
		-starts game in max(c0 ping, c1 ping) - my ping
		

		