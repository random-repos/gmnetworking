
//read the info
var eventId = ds_map_find_value(async_load, "id");
var eventType = ds_map_find_value(async_load,"type");
var eventBuffer = ds_map_find_value(async_load, "buffer");

if(eventId == mSocket)
{
    //show_debug_message("got message " + string(eventBuffer));
    if(eventType == network_type_data)
    {
        var message = ds_list_create();
        ds_list_read(message,buffer_read(eventBuffer,buffer_string));
        var msgKey = ds_list_find_value(message,0);
        if(msgKey == "clientidentity"){
            mIdentity = ds_list_find_value(message,1);
            if(ident == 0){
                mIsServer = true;
                //TODO create server
            }
        } else if (msgKey == "opponentip"){
            if(mIdentity == 0){
                //TODO tell router opponentip is an OK host
            } else {
                //TODO create client or spec
            }
        }
        ds_list_destroy(message);
    }
}
