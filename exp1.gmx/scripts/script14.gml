
var server = argument0;
var clientId = argument1;
var list = argument2;

//if this client exsits
if(ds_list_find_index(mClientList,clientId) >= 0)
{
    
    var buffer =  buffer_create(global.NETWORK_BUFFER_SIZE, buffer_fixed, 1);  //better make sure bufer is long enough...
    buffer_seek(buffer,buffer_seek_start,0);
    buffer_write(buffer,buffer_string,ds_list_write(list));
    
    with(server)
    {

        var sock = ds_list_find_value(mClientList,clientId);
        var ret = network_send_packet(sock, buffer, buffer_get_size(buffer));

    }
} else show_debug_message("ERROR sending to non existent client " + string(clientId));
