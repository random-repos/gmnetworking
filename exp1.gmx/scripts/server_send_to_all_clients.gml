var server = argument0;
var list = argument1;
var sourceSock = argument2;


var buffer =  buffer_create(global.NETWORK_BUFFER_SIZE, buffer_fixed, 1);  //better make sure bufer is long enough...
buffer_seek(buffer,buffer_seek_start,0);
buffer_write(buffer,buffer_string,ds_list_write(list));

with(server)
{

    for(i=0; i<ds_grid_height(mClientGrid); i++)
    {
        var sock = ds_grid_get(mClientGrid,0,i);
        if(sourceSock != sock){
            network_send_packet(sock, buffer, buffer_get_size(buffer));
        }
    }
}
buffer_delete(buffer);
