specclient = argument0;
message = argument1;

var buffer =  buffer_create(global.NETWORK_BUFFER_SIZE, buffer_fixed, 1); 
var pinglist = ds_list_create();
ds_list_add(pinglist,"ping_cs");
ds_list_add(pinglist,ds_list_find_value(message,1)); //pingid
ds_list_add(pinglist,0); //client time, //TODO
buffer_seek(buffer,buffer_seek_start,0);
buffer_write(buffer,buffer_string,ds_list_write(pinglist));
with(specclient){
    var ret = network_send_packet(mSocketId, other.buffer, buffer_get_size(other.buffer));
}

