
var server = argument0;
var buffer = argument1;
with(server)
{

    for(i=0; i<ds_list_size(mClientList); i++)
    {
        var sock = ds_list_find_value(mClientList,i);
        var ret = network_send_packet(sock, other.buffer, buffer_get_size(other.buffer));
    }
}

