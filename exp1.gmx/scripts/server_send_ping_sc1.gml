
server = argument0;
pingId = argument1;

with(server)
{
    var buffer =  buffer_create(global.NETWORK_BUFFER_SIZE, buffer_fixed, 1); 
    pinglist = ds_list_create();
    ds_list_add(pinglist,other.pingId);
    for(i=0; i<ds_list_size(mClientList); i++)
    {
        var sock = ds_list_find_value(mClientList,i);
        buffer_seek(buffer,buffer_seek_start,0);
        buffer_write(buffer,buffer_string,ds_list_write(pinglist));
        var ret = network_send_packet(sock, buffer, buffer_get_size(buffer));
    }
}

