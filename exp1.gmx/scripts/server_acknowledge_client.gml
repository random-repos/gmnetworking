var server = argument0;
var clientSocket = argument1;
var message = argument2;
var playerType = 0;

if(ds_list_find_value(message,1))
    playerType = 1; //should be 2 depending on # of ppl connected
    
with(server)
{
    show_debug_message("SERVER client register message from " + string(clientSocket));
    var clientIndex = find_client_by_socket(mClientGrid,clientSocket);
    if(clientIndex >= 0)
        ds_grid_set(mClientGrid,2,clientIndex,playerType);
    else
        show_debug_message("ERROR, attempting to register unkown client");
        
    var rmsg = ds_list_create()
    ds_list_add(rmsg,"clientack");
    ds_list_add(rmsg,ds_grid_get(mClientGrid,1,clientIndex));
    ds_list_add(rmsg,true);
    ds_list_add(rmsg,playerType);

    send_package(clientSocket,rmsg);
    
    ds_list_replace(rmsg,1,false);
    for(i=0; i<ds_grid_height(mClientGrid); i++)
    {
        var otherClientSocket = ds_grid_get(mClientGrid,0,i);
        if(clientSocket != otherClientSocket)
            send_package(otherClientSocket,rmsg);
    }
    
    ds_list_destroy(rmsg);
}

