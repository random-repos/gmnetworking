var server = argument0;
var clientSocket = argument1;

with(server)
{
    //TODO eventually check if client is already connected
    var nGridHeight = ds_grid_height(mClientGrid)+1;
    ds_grid_resize(mClientGrid,ds_grid_width(mClientGrid),nGridHeight);
    ds_grid_set(mClientGrid,0,nGridHeight-1,clientSocket);
    ds_grid_set(mClientGrid,1,nGridHeight-1,clientSocket); //using the socket id guarantees a unique id
    ds_grid_set(mClientGrid,2,nGridHeight-1,"na");
    show_debug_message("SERVER received client conneciton on socket " + string(clientSocket));
}
