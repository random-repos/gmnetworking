
var server = argument0;
var pingId = argument1;
var buffer =  buffer_create(global.NETWORK_BUFFER_SIZE, buffer_fixed, 1); 
var pinglist = ds_list_create();
ds_list_add(pinglist,"ping_sc1");
ds_list_add(pinglist,pingId);
buffer_seek(buffer,buffer_seek_start,0);
buffer_write(buffer,buffer_string,ds_list_write(pinglist));
with(server)
{

    for(i=0; i<ds_list_size(mClientList); i++)
    {
        var sock = ds_list_find_value(mClientList,i);
        var ret = network_send_packet(sock, other.buffer, buffer_get_size(other.buffer));
    }
}

