
//read the info
var mainSocket = ds_map_find_value(async_load, "id");
var eventBuffer = ds_map_find_value(async_load, "buffer");

//show_debug_message("SERVER received packet " + string(mainSocket) + " "); 
if (mainSocket == mServerId) { //server event
    var otherSocket = ds_map_find_value(async_load, "socket");
    var otherIp = ds_map_find_value(async_load, "ip");
    if (ds_map_find_value(async_load,"type") == network_type_connect)
    {
        if(mServerClient >= 0){
            show_debug_message("MASTER, client " + string(otherSocket) + " " + string(otherIp) + " connected.");
            var package = ds_list_create();
            ds_list_add(package, "opponentip");
            ds_list_add(package, mServerClientIp);
            send_package(package,otherSocket);
        } else {
            mServerClient = otherSocket;
            mServerClientIp = otherIp;
            show_debug_message("MASTER, server client " + string(otherSocket) + " " + string(otherIp) +  " connected.");    
            var package = ds_list_create();
            ds_list_add(package, "youareserver");
            send_package(package,otherSocket);
        }
    }
    
    else if (ds_map_find_value(async_load,"type") == network_type_disconnect)
    {
        var cIndex = ds_list_find_value(mClientList,otherSocket);
        if(cIndex >= 0)
        {
           mServerClient = -1;
           mServerClientIp = 0;
           show_debug_message("MASTER, server client " + string(otherSocket) + " disconnected.");
        } else {
            show_debug_message("MASTER, client " + string(otherSocket) + " disconnected.");
        }
    }
} else {
    if(ds_list_find_value(mClientList,mainSocket) >= 0) //if it's from a known client
    {
        var message = ds_list_create();
        var raw = buffer_read(eventBuffer,buffer_string);
        ds_list_read(message,raw);
        ds_list_destroy(message);
    }
}
