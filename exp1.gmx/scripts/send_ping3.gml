
var server = argument0;
var clientSocket = argument1;
var message = argument2;
var pingId = ds_list_find_value(message,1);
var clientPing = 0; 
var clientId;
with(server){
    other.clientPing = get_timer()-ds_map_find_value(mPingIdClientTimeMap,pingId);
    ds_map_delete(mPingIdClientTimeMap,pingId);
}
var pinglist = ds_list_create();
ds_list_add(pinglist,"ping3");
ds_list_add(pinglist,pingId);
ds_list_add(pinglist,clientSocket);
ds_list_add(pinglist,clientPing); 
ds_list_add(pinglist,ds_list_find_value(message,2) - clientPing/2);
//show_debug_message("sending pingid " + string(pingId));
server_send_to_all_clients(server,pinglist);

ds_list_destroy(pinglist);
