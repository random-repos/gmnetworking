
//read the info
var eventId = ds_map_find_value(async_load, "id");
var eventType = ds_map_find_value(async_load,"type");
var eventBuffer = ds_map_find_value(async_load, "buffer");

if(eventId == mSocket)
{
    //show_debug_message("got message " + string(eventBuffer));
    if(eventType == network_type_data)
    {
        var message = ds_list_create();
        ds_list_read(message,buffer_read(eventBuffer,buffer_string));
        var msgKey = ds_list_find_value(message,0);
        if(msgKey == "ping1"){
            send_ping2(self,message);
        } else if (msgKey == "ping3") {
            //we know our lag now TOdO
            show_debug_message("client " + 
                string(ds_list_find_value(message,2)) + 
                " pingid " + 
                string(ds_list_find_value(message,1)) + 
                " time " + 
                string(ds_list_find_value(message,3)));
        } else if (msgKey == "clientack") {
            if(ds_list_find_value(message,2) == true){
                mRemoteClientId = ds_list_find_value(message,1);
                show_debug_message("CLIENT acknowledged, remote id is " + string(mRemoteClientId));
            }
            //TODO potential for client to be rejected by server, then need to restart client object
        }
        ds_list_destroy(message);
    }
}
