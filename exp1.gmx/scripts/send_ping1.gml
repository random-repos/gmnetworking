
var server = argument0;
var pinglist = ds_list_create();
var pingId = 0;
with(server){
    pingId = mLastPingId;
    mLastPingId = mLastPingId + 1;
    ds_map_add(mPingIdClientTimeMap,mLastPingId,get_timer());
}
ds_list_add(pinglist,"ping1");
ds_list_add(pinglist,pingId);
server_send_to_all_clients(server,pinglist,-1);
ds_list_destroy(pinglist);
