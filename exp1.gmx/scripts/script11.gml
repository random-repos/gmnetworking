var client = argument0;
var list = argument1;

var buffer =  buffer_create(global.NETWORK_BUFFER_SIZE, buffer_fixed, 1);  //better make sure bufer is long enough...
buffer_seek(buffer,buffer_seek_start,0);
buffer_write(buffer,buffer_string,ds_list_write(list));

with(client)
{
    network_send_packet(mSocketId, other.buffer, buffer_get_size(other.buffer));
}

