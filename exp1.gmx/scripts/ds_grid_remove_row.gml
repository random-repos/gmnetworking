var grid = argument0;
var row = argument1;

var gridWidth = ds_grid_width(grid);
var gridHeight = ds_grid_height(grid);

for(var i = row+1; i < gridHeight; i++)
{
    for(var j = 0; j < gridWidth; j++)
    {
        ds_grid_set(grid,i-1,j, ds_grid_get(grid,i,j));
    }
}
ds_grid_resize(grid,gridWidth,gridHeight);

