
//read the info
var mainSocket = ds_map_find_value(async_load, "id");
var eventBuffer = ds_map_find_value(async_load, "buffer");

//show_debug_message("SERVER received packet " + string(mainSocket) + " "); 
if (mainSocket == mServerId) { //server event
    var otherSocket = ds_map_find_value(async_load, "socket");
    if (ds_map_find_value(async_load,"type") == network_type_connect)
    {
        server_add_client(self,otherSocket);
    }
    
    else if (ds_map_find_value(async_load,"type") == network_type_disconnect)
    {
        ds_grid_remove_row(mClientGrid,find_client_by_socket(mClientGrid,otherSocket));
        show_debug_message("SERVER client disconnected on socket " + string(otherSocket));
    }
} else {
    if(find_client_by_socket(mClientGrid,mainSocket) >= 0) //if it's from a known client
    {
        var message = ds_list_create();
        var raw = buffer_read(eventBuffer,buffer_string);
        //show_debug_message("SERVER receieved message from " + string(mainSocket) + " msg: " + raw);
        ds_list_read(message,raw);
//        for(i = 0; i < ds_list_size(output);i++){
//            show_debug_message(string(ds_list_find_value(output,i)));
//        }
        
        var msgKey = ds_list_find_value(message,0);
        if(msgKey == "ping2"){
            send_ping3(self,mainSocket,message);
        } else if(msgKey == "clientregister"){
            server_acknowledge_client(self,mainSocket,message);
        } else {
            server_send_to_all_clients(self,message,mainSocket);
        }
        ds_list_destroy(message);
    }
}
